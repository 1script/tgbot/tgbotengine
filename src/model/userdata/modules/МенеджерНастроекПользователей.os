
Перем Настройки;

Перем ПутьХраненияНастроек;

Функция ПолучитьНастройки(ПользовательID) Экспорт
	
	Если НЕ ЗначениеЗаполнено(ПользовательID) Тогда
		НастройкиПриложения.Лог.КритическаяОшибка("Пустой ID пользователя");
		Возврат Неопределено;
	КонецЕсли;
	
	НастройкиПользователя = Настройки[ПользовательID];
	Если НастройкиПользователя <> Неопределено Тогда
		НастройкиПользователя.Прочитать();
		Возврат НастройкиПользователя;
	КонецЕсли;
	
	НастройкиПользователя = Новый НастройкиПользователя(ПользовательID);
	Настройки.Вставить(ПользовательID, НастройкиПользователя);	
	Возврат НастройкиПользователя;

КонецФункции

Функция КаталогПользователя(ПользовательID) Экспорт

	Поз = СтрНайти(ПользовательID, "/", НаправлениеПоиска.СКонца);
	Если Поз > 0 Тогда
		Путь = Лев(ПользовательID, Поз-1);
	Иначе
		Путь = Строка(ПользовательID);
	КонецЕсли;

	Возврат ОбъединитьПути(ПутьХраненияНастроек, Путь);

КонецФункции

Функция ПутьКФайлуНастроекПользователя(ПользовательID) Экспорт

	Поз = СтрНайти(ПользовательID, "/", НаправлениеПоиска.СКонца);
	Если Поз > 0 Тогда
		ИмяФайла = Сред(ПользовательID, Поз+1);
	Иначе
		ИмяФайла = "user_" + Строка(ПользовательID);
	КонецЕсли;

	Возврат ОбъединитьПути(КаталогПользователя(ПользовательID), ИмяФайла + ".json");

КонецФункции

Функция ПолучитьЗначениеНастройки(ПользовательID, ИмяСвойства) Экспорт

	НастройкиПользователя = ПолучитьНастройки(ПользовательID);

	Если СтрНайти(ИмяСвойства, "|") = 0 Тогда

		Значение = НастройкиПользователя.ПолучитьЗначение(ИмяСвойства); 
		Возврат Значение;

	Иначе

		Источник = НастройкиПользователя.НастройкиПользователя();
		мКонвейерИменСвойств = СтрРазделить(ИмяСвойства, "|", Ложь);
		Для Каждого ИмяСвойстваИсточника Из мКонвейерИменСвойств Цикл
			Значение = Источник[ИмяСвойстваИсточника];
			Если Значение = Неопределено Тогда
				Возврат Неопределено;
			Иначе
				Источник = Значение;
			КонецЕсли;
		КонецЦикла;

		Возврат Значение;
	
	КонецЕсли;

КонецФункции

Функция ПолучитьЗначенияНастройки(ПользовательID, Ключи=Неопределено) Экспорт
	
	НастройкиПользователя = ПолучитьНастройки(ПользовательID);

	Если ТипЗнч(Ключи) = Тип("Строка") Тогда
		мКлючи = СтрРазделить(Ключи, ",; ", Ложь);
	ИначеЕсли ТипЗнч(Ключи) = Тип("Массив") Тогда
		мКлючи = Ключи;
	ИначеЕсли Ключи = Неопределено Тогда
		мКлючи = Ключи;
	Иначе
		НастройкиПриложения.Лог.КритическаяОшибка("Передача Ключей через тип '%1' не реализована", ТипЗнч(Ключи));
	КонецЕсли;

	Соответствие = НастройкиПользователя.ПолучитьЗначения(мКлючи); 
	
	Возврат Соответствие;

КонецФункции

Процедура УстановитьЗначениеНастройки(ПользовательID, ИмяСвойства, Значение) Экспорт
	НастройкиПользователя = ПолучитьНастройки(ПользовательID);
	НастройкиПользователя.УстановитьЗначение(ИмяСвойства, Значение);
КонецПроцедуры

Процедура УстановитьЗначенияНастройки(ПользовательID, ПарыКлючЗначение) Экспорт
	НастройкиПользователя = ПолучитьНастройки(ПользовательID);
	НастройкиПользователя.УстановитьЗначения(ПарыКлючЗначение);
КонецПроцедуры

Процедура СохранитьПользователя(Пользователь) Экспорт
	
	Поля = "id,first_name,last_name,username,language_code";
	
	соотв = ПолучитьЗначениеНастройки(Пользователь["id"], "user");
	
	Если соотв = Неопределено Тогда
		ЕстьИзменения = Истина;
		
	Иначе
		ЕстьИзменения = Ложь;
		Для Каждого КЗ Из соотв Цикл
			Если КЗ.Значение <> Пользователь[КЗ.Ключ] Тогда
				ЕстьИзменения = Истина;
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

	Если ЕстьИзменения Тогда
		НастройкиПриложения.Лог.Информация("Обновлена информация пользователя %1", Пользователь["id"]);
		МенеджерНастроекПользователей.УстановитьЗначениеНастройки(Пользователь["id"], "user", Пользователь);
	КонецЕсли;

КонецПроцедуры

Функция ПолучитьВсехПользователей() Экспорт
	мФайлы = НайтиФайлы(ПутьХраненияНастроек, "config_*.json", Истина);
	массивПользовательID = Новый Массив;
	Для каждого Файл Из мФайлы Цикл
		ID = СтрЗаменить(Файл.ИмяБезРасширения, "config_", "");
		Попытка
			массивПользовательID.Добавить(Число(ID));
		Исключение
			НастройкиПриложения.Лог.Ошибка("Не удалось определить ID пользователя. %1 не является числом", ID);
		КонецПопытки;
	КонецЦикла;
	Возврат массивПользовательID;
КонецФункции

Настройки = Новый Соответствие;
ПутьХраненияНастроек = ОбъединитьПути(".", "wwwroot", "users");